//importing packages
const express = require('express');
const cors = require('./middlewares/cors');
const mongoos = require('mongoose');
const bodyParser = require('body-parser');
const morgan = require('morgan')
const path = require('path')
const io = require('./socket/io-server').io;

//route middlewares
const authMiddleware = require('./middlewares/auth');

/**
 * importing routes
 */
//user route
const userRoutes = require('./user/routes');
//post route
const postRoutes = require('./posts/routes.js');

/**
 * Initializations
 */
//creating connection to mongoDB
mongoos.connect('mongodb://dahmed:knust2015@chat-room-shard-00-00-49d0s.mongodb.net:27017,chat-room-shard-00-01-49d0s.mongodb.net:27017,chat-room-shard-00-02-49d0s.mongodb.net:27017/test?ssl=true&replicaSet=chat-room-shard-0&authSource=admin&retryWrites=true')
.then(result => {
    console.log('mongodb connected')
}).catch(err => {
    console.log('oops, error while connecting to mongodb, connection terminated', err)
})


/**
 * creating express app instance
 */
const app = express();

/**
 * Applying middlewares
 */
//cors middleware
// console.log(path.join(__dirname, 'storage'))
app.use('/profile', express.static(path.join(__dirname, 'storage/disk/profile')));
app.use(cors);
app.use(morgan('dev'))
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())


/**
 * using routes
 */
//user route
app.use('/user', userRoutes);
app.use('/post', authMiddleware, postRoutes);
app.get('/test/:m', function (req, res) {
    // io.emit('welcome', req.params.m)
    res.status(200).json({message: req.params.m})
})

// app.use((request, response, next) => {
//     response.status(404).json({
//         message: 'Page not found'
//     })
// })

app.use(function (err, req, res, next) {
    console.error(err.stack)
    res.status(500).json({
        message: 'something unexpected happended',
        error: err.stack
    })
})




//exporting express app
module.exports = app