const jsonWebToken = require('jsonwebtoken');
const env = require('./../env');

module.exports = (request, response, next) => {
    const authorization = request.headers.authorization;
    let token = null;
    if (authorization) {
        const arr = request.headers.authorization.split(' ')
        if (arr.length == 2) {
            token = arr[1];
        }
    }
    try{
        const decoded = jsonWebToken.verify(token, env.JWT_SECRET_KEY);
        request.auth = {id: decoded.id, email: decoded.email, username: decoded.username};
        // console.log(request.auth)
        next()
    }catch(err) {
        response.status(401).json({
            message: 'Authorization Failed'
        })
    }
}