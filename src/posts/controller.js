const Post = require('./model.js')
const mongoose = require('mongoose')

exports.create = (request, response) => {
    if (request.body.content) {
        const post = new Post({
            _id: mongoose.Types.ObjectId(),
            author: request.auth.id,
            postedOn: (new Date()).toDateString(),
            content: request.body.content
        })
        // response.json(post)
        post.save().then(doc => {
            Post.findById(doc._id).populate('author').exec().then(result => {
                response.status(200).json({message: 'Post created', post: result})
            })
        }).catch(err => {
            response.status(500).json({message: 'Error occured while save post', error: err})
        })
    } else {
        response.status(200).json({message: 'Post content required'})
    }
}

exports.posts = (request, response) => {
    // global.io.sockets.emit('hello', 'hello message from postController')
    Post.find().populate('author').exec().then(docs => {
        response.status(200).json(docs)
        // console.log('get all posts')
        // console.log(docs)
    }).catch(err => {
        response.status(500).json(err)
    })
}

exports.deleteAll = (request, response) => {
    Post.find().exec().then(posts => {
        for (let post of posts) {
            // console.log(user._id)
            Post.deleteOne({_id: post._id}).exec().then(result => {
                console.log('deleted ')
            }).catch(err => {
                console.log(err)
            })
        }
    })
    response.status(200).json({message: 'posts deleted'})
}