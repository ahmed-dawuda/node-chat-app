const mongoose = require('mongoose')
const Schema = mongoose.Schema

const PostSchema = new Schema({
    _id: {type: mongoose.Schema.Types.ObjectId, required: true},
    author: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
    postedOn: {type: String, required: true},
    content: {type: String, required: true}
});

module.exports = mongoose.model('Post', PostSchema);