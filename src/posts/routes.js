const router = require('express').Router();
const postController = require('./controller.js');

router.post('/', postController.create);
router.get('/', postController.posts);
router.delete('/', postController.deleteAll);

module.exports = router