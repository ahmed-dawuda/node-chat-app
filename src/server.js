//requiring http package to create the server
const http = require('http');
//requiring express.js app
const app = require('./app');
//requiring socket.io
const initIO = require('./socket/io-server').initIO;

const PORT = process.env.PORT || 3000;



//creating server
const server = http.createServer(app)

//wiring up io to server
initIO(server);

//listening for server listening
server.on('listening', function () {
    console.log('server started and listening on port 3000')
})

//listening for any errors during server initialization
server.on('error', function (error) {
    console.log(error)
})

//making server listen at the given port
server.listen(PORT)
