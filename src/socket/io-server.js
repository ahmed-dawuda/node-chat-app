const socketIO = require('socket.io');
let io;

const init = (server) => {
    io = socketIO(server);

    io.on('connection', function(socket) {
        // Tell other users of the new user
        socket.broadcast.emit('welcome', 'A new user has connected')

        //send new user to toher users
        socket.on('new-user', function(data) {
            socket.broadcast.emit('new-user', data)
        })

        socket.on('send-chat-room-message', function(data) {
            socket.broadcast.emit('recieve-chat-room-message', data)
        })

        // io.sockets.emit('hello', 'hello message')

        socket.on('server', (data) => {
            console.log(data)
        } )

        global.io = io
    })
}

module.exports = {initIO: init, io: io}