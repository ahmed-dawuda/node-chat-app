module.exports = (io) => {
    io.on('connection', function (client) {
        console.log('new user connected');
        client.emit('welcome', 'Welcome message from node.js');
    })

    io.on('disconnect', function (socket) {
        console.log('user disconnected');
    })
}