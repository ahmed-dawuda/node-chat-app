const multer = require('multer');

const profileStorage = multer.diskStorage({
    destination: 'src/storage/disk/profile/',
    filename: (req, file, cb) => {
        cb(null, file.fieldname + Date.now() + file.originalname);
    }
})

exports.profileStorage = (filterFunction) => {
    const upload = multer({storage: profileStorage, fileFilter: filterFunction});
    return upload;
}