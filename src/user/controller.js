const env = require('../env.js');
const jsonWebToken = require('jsonwebtoken');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const Validator = require('validatorjs');
const fs = require('fs')
//importing user model
const User = require('./model.js');


exports.register = (request, response) => {
    User.find({email: request.body.email}).exec().then(users => {
        if (users.length > 0) {
            response.status(422).json({
                message: 'Registration unsuccessful',
                errors: {
                    email: ['Email is already taken']
                }
            })
        } else {
            //hashing the password before saving
            bcrypt.hash(request.body.password, 10).then(hash => {
                //on successful hashing, create new user model
                const user = new User({
                    _id: mongoose.Types.ObjectId(),
                    email: request.body.email,
                    username: request.body.username,
                    password: hash
                });
                //persisting the model
                user.save().then(result => {
                    //sending response to client
                    response.status(200).json({message: 'user registered', user: {
                        id: result._id, username: result.username, email: user.email, image: result.image
                    }})
                }).catch(err => {
                    response.status(500).json({
                        message: 'Oops, Something unexpected occured, there is an internal server error',
                        error: err
                    })
                })
            }).catch(err => {
                response.status(500).json({
                    message: 'Oops, Something unexpected occured, there is an internal server error',
                    error: err
                })
            })
        }
    }).catch(err => {
        response.status(500).json({message: 'internal server error', error: err})
    })
}

exports.login = (request, response) => {
    //extract user login credentials
    const credentials = {email: request.body.email, password: request.body.password};

    //find and authenticate user
    User.findOne({email: credentials.email}).exec().then(user => {
        bcrypt.compare(credentials.password, user.password).then(result => {
            if (result) {
                const token = jsonWebToken.sign({id: user._id, email: user.email, username: user.username}, env.JWT_SECRET_KEY, {expiresIn: '24h'});
                response.status(200).json({message: 'Authentication successful', token: token, 
                user: {
                    id: user._id,
                    email: user.email, 
                    username: user.username,
                    image: user.image
                }
            })
            } else {
                response.status(401).json({message: 'Authentication Failed'})
            }
        })
    }).catch(err => {
        response.status(401).json({message: 'Authentication Failed'})
    })
}

exports.user = (request, response) => {
    User.findById(request.params.id).exec().then(user => {
        response.status(200).json({message: 'user details', user: user})  
    })
}

exports.update = (request, response) => {
    // response.status(200).json(request.body)
    const img = request.file ? request.file.path : null;
    // response.status(200).json({message: 'ok'})

    const validation = new Validator({
        email: request.body.email,
        password: request.body.password,
        username: request.body.username
    }, {
        email: 'required|email',
        password: 'required',
        username: 'required'
    })

    // console.log(request.file)

    if (validation.passes()) {
        let object = {
            email: request.body.email,
            username: request.body.username
        }
        if (img) {
            const imgs = img.split('\\');
            // console.log(imgs)
            object.image = '/' + imgs[3] + '/' +  imgs[4];
        }
        // response.json(object)

        // console.log(typeof request.body.newPassword)
    
        if (request.body.newPassword) {
            // console.log('setting password to ' + request.body.newPassword)
            object.password = bcrypt.hashSync(request.body.newPassword, 10);
        }

        // response.json(object)
        // console.log(request.body)
        User.findOne({email: request.body.email}).exec().then(user => {
            if (user) {
                // console.log(user._id)
                // console.log(request.body.id)
                if (user._id != request.body.id) {
                    response.status(422).json({message: 'Oops, this email is not available'})
                } else {
                    bcrypt.compare(request.body.password, user.password).then(result => {
                        if (result) {
                            User.findByIdAndUpdate(user._id, object, {new: true}).exec().then((doc, err) => {
                                if (doc) {
                                    response.status(200).json({message: 'Profile updated', user: doc})
                                } else {
                                    response.status(500).json({message: 'Something unexpected happened', error: err})
                                }
                            })
                        } else {
                            response.status(401).json({message: 'You do not have permission to update profile'})
                        }
                    })
                }
            } else {
                User.findById(request.body.id).exec().then(auth => {
                    if (auth) {
                        bcrypt.compare(request.body.password, auth.password).then(result => {
                            if (result) {
                                User.findByIdAndUpdate(auth._id, object, {new: true}).exec().then(doc => {
                                    response.status(200).json({message: 'Profile updated', user: doc})
                                }).catch(err => {
                                    response.status(500).json({message: 'Something unexpected happened', error: err})
                                })
                            }
                        })
                    } else {
                        response.status(401).json({message: 'You are not authorized to use the app. Please consider registering'})
                    }
                })
            }
        }).catch(err => {
            response.status(500).json(err)
        })

    } else {
        response.status(422).json({
            message: 'Invalid data error',
            errors: validation.errors
        })
    }
}

exports.deleteUser = (request, response) => {
    User.find().exec().then(users => {
        for (let user of users) {
            // console.log(user._id)
            User.deleteOne({email: user.email}).exec().then(result => {
                console.log('deleted ' + user.email)
            }).catch(err => {
                console.log(err)
            })
        }
    })
    response.status(200).json({message: 'users deleted'})
}