//creating user router instance
const router = require('express').Router();
const multer = require('multer');
const Validator = require('validatorjs');
const Storage = require('./../storage')
const authMiddleware = require('./../middlewares/auth');

const filter = (req, file, cb) => {
    // console.log(req.body.id)
    let validation = new Validator(req.body, {
        email: 'required|email',
        username: 'required',
        password: 'required|min:6'
    })

    if (validation.passes()) {
        if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpg' || file.mimetype === 'image/png' || file.mimetype === 'image/gif') {
            cb(null, true);
        } else {
            cb(null, false)
        }   
    } else {
        cb(null, false)
    }

}

//importing userController
const userController = require('./controller.js')

router.post('/register', userController.register);
router.post('/login', userController.login);
router.get('/:id', userController.user);
router.put('/', authMiddleware, Storage.profileStorage(filter).single('image'), userController.update);
router.delete('/', userController.deleteUser);

//exporting user router
module.exports = router